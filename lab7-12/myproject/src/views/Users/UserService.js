const userService = {
  userList: [
    {
      id: 1,
      name: 'Weerasak',
      gender: 'M'
    },
    {
      id: 2,
      name: 'Kirito',
      gender: 'M'
    },
    {
      id: 3,
      name: 'Suban',
      gender: 'F'
    }
  ],
  lastId: 4,
  addUser (user) {
    user.id = this.lastId++
    this.userList.push(user)
  },
  updateUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1, user)
  },
  deleteUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1)
  },
  getUsers () {
    return [...this.userService]
  }
}

export default userService
