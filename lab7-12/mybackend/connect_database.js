const mongoose = require('mongoose')
const User = require('./models/User')

mongoose.connect('mongodb://admin:1234@localhost/mydb', { useNewUrlParser: true, useUnifiedTopology: true })

const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error'))
db.once('open', () => {
  console.log('connect')
})

// User.insertMany([{name:'Tester',gender:'F'}]).then((user) =>{
//   console.log('add user complete')
// })