const User = require('../models/User')
const userController = {
  addUser(req, res, next) {
    const payload = req.body
    User.insertMany(payload).then((user) => {
      res.json(user)
    }).catch((err) => {
      res.status(500).send('error')
    })
  },
  updateUser(req, res, next) {
    const payload = req.body
    console.log(payload)
    User.updateOne({ _id: payload._id }, payload).then((stat) => {
      res.json(stat)
    }).catch((err) => {
      res.status(500).send('error')
    })
  },
  deleteUser(req, res, next) {
    const { id } = req.params
    User.deleteOne({ _id: id }).then(() => {
      res.send('Delete ID ' + id + ' Success')
    }).catch((err) => {
      res.status(500).send('error')
    })
  },
  getUsers(req, res, next) {
    User.find({}).then((users) => {
      res.json(users)
    }).catch((err) => {
      res.status(500).send('error')
    })
    // res.json(userController.getUsers())
  },
  getUser(req, res, next) {
    const { id } = req.params
    User.findById(id).then((users) => {
      res.json(users)
    }).catch((err) => {
      res.status(500).send('error')
    })
  }
}

module.exports = userController
